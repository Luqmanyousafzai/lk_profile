import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Project } from '../interfaces/project.interface';

@Injectable({
  providedIn: 'root'  
})
export class ProjectsService {

  projects: Project[] = [
    {
        projectName: "PatronWorks",
        image:'../../../assets/img/patronworks.jpeg',
        url: "https://patronworks.com/",
        date: "2021-08-21",
        category: "Point of Sale Software",
        gitlabLink:"https://patronworks.com/"
    },
    {
        projectName: "Patronpal",
        url: "https://patronpal.com/",
        date: "2023-07-15",
        category: "Restaurant Management System",
        gitlabLink:"https://patronpal.com/home",
        image:'../../../assets/img/patronpal.png',

    },
    {
        projectName: "Maksaem Engineering",
        url: "https://maksaemengineering.com/",
        date: "2022-06-10",
        category: "Online Services",
        gitlabLink:"https://maksaemengineering.com/",
        image:'../../../assets/img/maksaem.png',

    },
    {
      projectName: "OneRunCeU",
      url: "https://onerunceu.patronpal.com/",
      date: "2022-06-10",
      category: "Online course registration",
      gitlabLink:"https://onerunceu.patronpal.com/",
      image:'../../../assets/img/Onerunceu.png',
      

  },
  
  {
    projectName: "Portfolio",
    url: "https://luqmandeveloper.netlify.app/",
    date: "2024-01-10",
    category: "Online Services",
    gitlabLink:"https://luqmandeveloper.netlify.app/",
    image:'../../../assets/img/portfolio.png',
    

},
{
  projectName: "Patrolpump",
  url: "https://patrolpump.netlify.app/auth",
  date: "2024-08-04",
  category: "Patrolpump MAnagement System",
  gitlabLink:"https://patrolpump.netlify.app/auth",
  image:'../../../assets/img/patrolpump.png',
  

},

  
];


  constructor( private http: HttpClient ) {
    // this.loadProjects();
   }


  //  private loadProjects() {
  //   this.http.get('https://portfolio-tem.firebaseio.com/projects_idx.json')
  //     .subscribe( (resp: Project[]) => {
  //       this.projects = resp;
  //     });
  //  }
}
