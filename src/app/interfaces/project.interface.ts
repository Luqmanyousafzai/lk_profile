export interface Project {
    projectName: string; // Project name
    url: string;         // URL
    date: string;        // Date
    category: string;    // Category
    gitlabLink:string;
    image:string;
}
